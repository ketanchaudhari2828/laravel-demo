<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table= 'product';

    protected $appends = ['product_image_url'];

    public function getProductImageUrlAttribute() {
        return asset('uploads/'.$this->product_image);
    }
}
