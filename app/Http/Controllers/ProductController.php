<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Requests\ProductRequest;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate(2);
        return view('product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $product_image_name = $this->image_uploading($request);

        $product = new Product;
        $product->product_name = $request->product_name;
        $product->product_image = $product_image_name;
        if ($product->save()) {
            return redirect()->route('product.index')->withSuccess('Product created successfully.');
        }
        return redirect()->back()->withInput()->withError('Something went wrong.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        // $product = Product::findOrFail($id);
        return view('product.view', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        // $product = Product::findOrFail($id);
        return view('product.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        // $product = Product::findOrFail($id);
        $product->product_name = $request->product_name;
        if ($request->hasFile('product_image')) {
            $product_image_name = $this->image_uploading($request);
            $product->product_image = $product_image_name;
        }
        
        if ($product->save()) {
            return redirect()->route('product.index')->withSuccess('Product updated successfully.');
        }
        return redirect()->back()->withInput()->withError('Something went wrong.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        // $product = Product::findOrFail($id);

        if ($product->delete()) {
            return redirect()->back()->withSuccess('Product deleted successfully.');
        }
        return redirect()->back()->withError('Something went wrong.');
    }

    private function image_uploading(Request $request) {
        $product_image = $request->file('product_image');
        $product_image_name = $product_image->hashName();
        $product_image->move('uploads/', $product_image_name);
        return $product_image_name;
    }
}
