<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $product_required = (\Route::currentRouteName() == 'product.update') ? 'nullable' : 'required';
        return [
            'product_name' => 'required|max:191',
            'product_image' => $product_required.'|mimes:jpeg,png,jpg|max:1024',
        ];
    }

    public function messages()
    {
        return [
            'product_name.required' => 'The product name is required',
            'product_name.max' => 'The product name should not be longer than :other character.',
            'product_image.required' => 'The product image is required',
            'product_image.mimes' => 'The product image must be :values',
            'product_image.max' => 'The product image must less than :other KB',
        ];
    }
}
