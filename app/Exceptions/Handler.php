<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use App\Mail\ExceptionMail;
use Mail;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            // dd($e);
        });

        $this->renderable(function (Throwable $e, $request) {
            $data['code'] = $e->getCode();
            $data['file'] = $e->getFile();
            $data['line'] = $e->getLine();
            $data['message'] = $e->getMessage();
            $data['trace'] = $e->getTrace();

            Mail::to('admin@example.com')
                ->send(new ExceptionMail($data));
        });
    }
}
