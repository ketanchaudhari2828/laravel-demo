@extends('layouts.main')

@section('content')
<div class="col-4 offset-2">
    <h2>Product Form</h2>
    
    @include('layouts.success_error')

    <form action="{{ route('product.store') }}" method="post" enctype="multipart/form-data">
    	@csrf
    	@method('post')
        @include('product._form')
        <button type="submit" class="btn btn-primary">Create</button>
    </form>
</div>
<div class="col-2 mt-5">
    <div>
        <label for="product_image_preview">Product Image Preview:</label>
        <img src="https://via.placeholder.com/150" class="img-thumbnail img-fluid" alt="Product Image"  style="height: 150px;width: 150px;">
    </div>
</div>
@endsection