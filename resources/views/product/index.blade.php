@extends('layouts.main')

@section('content')
<div class="col-8 offset-2">
    <h2>Product List</h2>
    <div>
        <a href="{{ route('product.create') }}" class="btn btn-primary float-end mb-2">Create</a>    
    </div>

    @include('layouts.success_error')

    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Product Image</th>
                <th>Product Name</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($products as $index => $product)
            <tr>
                <td>{{ $products->firstItem()  + $index }}</td>
                <td>
                    <img src="{{ $product->product_image_url }}" class="img-thumbnail img-fluid" alt="Product Image"  style="height: 80px;width: 80px;">
                </td>
                <td>{{ $product->product_name }}</td>
                <td>
                    <a class="btn btn-info" title="View" href="{{ route('product.show', ['product' => $product->id]) }}">
                        View
                    </a>
                    <a class="btn btn-primary" title="Edit" href="{{ route('product.edit', ['product' => $product->id]) }}">
                        Edit
                    </a>
                    <button class="btn btn-danger" title="Delete" onclick="if (confirm('Are you sure want to delete?')) { $(this).siblings('form').submit(); }">
                        Delete
                    </button>
                    <form action="{{ route('product.destroy', ['product' => $product->id]) }}" method="post">
                        @csrf
                        @method('delete')
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $products->links() }}
</div>
@endsection