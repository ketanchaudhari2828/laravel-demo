@extends('layouts.main')

@section('content')
<div class="col-6 offset-2">
    <h2>Product View</h2>
    <div>
        <label class="fw-bold">Product Name : </label>
        <div>
            {{ $product->product_name }}
        </div>
    </div>
    <div class="mt-3">
        <label class="fw-bold">Product Image : </label>
        <div>
            <img src="{{ $product->product_image_url }}" class="img-thumbnail img-fluid" alt="Product Image"  style="height: 150px;width: 150px;">
        </div>
    </div>
    <div class="mt-3">
        <div>
            <a class="btn btn-danger" href="{{ route('product.index') }}">Go Back</a>
        </div>
    </div>
</div>
@endsection