<div class="mb-3 mt-3">
    <label for="product_name">Product Name:</label>
    <input type="text" class="form-control" id="product_name" placeholder="Enter product name" name="product_name" value="{{ old('product_name', @$product->product_name) }}">
    <p class="text-danger">{{ $errors->first('product_name') }}</p>
</div>
<div class="mb-3">
    <label for="pwd">Product Image:</label>
    <input type="file" class="form-control" id="product_image" name="product_image">
    <p class="text-danger">{{ $errors->first('product_image') }}</p>
</div>