<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Exception</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container mt-3">
            <h2>Bordered Table</h2>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Type</th>
                        <th>Data</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Code</td>
                        <td>{{ $data['code'] }}</td>
                    </tr>
                    <tr>
                        <td>file</td>
                        <td>{{ $data['file'] }}</td>
                    </tr>
                    <tr>
                        <td>line</td>
                        <td>{{ $data['line'] }}</td>
                    </tr>
                    <tr>
                        <td>message</td>
                        <td>{{ $data['message'] }}</td>
                    </tr>
                </tbody>
            </table>

            <div>
                <pre>
                    @foreach($data['trace'] as $trace)
                        {{ json_encode($trace) }}
                    @endforeach
                </pre>
            </div>
        </div>
    </body>
</html>